
section .text

%define EXIT 60 
%define NULL_TERMINATION 0
%define STDOUT 1
%define WRITE 1
%define NEWLINE 0xA 
%define MIN '0'
%define MAX '9'
%define ASCII_MINUS '-'
%define ASCII_PLUS '+'
%define SPACE 0x20
%define TAB 0x9

 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT
    syscall     
    
   
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
               cmp byte [rdi+rax], NULL_TERMINATION
		je .end
		inc rax
		jmp .loop
		
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdx, rax
    mov rdi, STDOUT
    mov rax, WRITE
    syscall
    ret


; Принимает строку (выводит симбол с кодом 0xA)
print_newline:
   mov rdi, NEWLINE
   


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, WRITE
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    js .negative_case
    .positive_case:
      jns print_uint
    .negative_case:
        push rdi
	mov rdi, ASCII_MINUS
	call print_char
	pop rdi
	neg rdi

   
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.


print_uint:
   mov rax, rdi
   mov rdi, rsp
   mov r8, 10
   sub rsp, 32
   dec rdi
   mov byte [rdi], NULL_TERMINATION
  .loop:
    xor rdx, rdx
    div r8
    add rdx,  MIN
    dec rdi
    mov byte[rdi], dl
    and rax, rax
    jne .loop
  
   call print_string
    add rsp, 32
    ret
   
           


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor r8, r8
    xor r9, r9
  
    .loop:
        mov r9b, byte[rdi + rcx]
        mov r8b, byte[rsi + rcx]
        cmp r9b, r8b
        jne .not_eq
        test r9b, r9b
        je .eq
        inc rcx
        jmp .loop
             
   .eq:
        mov rax, 1
        ret

    .not_eq:
        xor rax, rax
        ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    mov rsi, rsp
    xor rdi, rdi
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

      
read_word:
    xor rdx, rdx

.control_1:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi 

    test rax, rax
    je .success
    cmp rdx, rsi
    je .failure
    test rdx, rdx
    jne .control_2

    cmp rax, SPACE
    je .control_1
    cmp rax, TAB
    je .control_1
    cmp rax, NEWLINE
    je .control_1


.control_2:
    cmp rax, SPACE
    je .success
    cmp rax, TAB
    je .success
    cmp rax, NEWLINE
    je .success

    mov [rdi+rdx], rax 
    inc rdx 
    jmp .control_1

.success:
    mov byte[rdi+rdx], 0 
    mov rax, rdi
    ret

.failure:
    xor rax, rax
    ret









; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8
   .loop:
      mov r8b, byte[rdi+rcx]
      cmp r8b, MIN
      jl .end
      cmp r8b, MAX
      jg .end
      imul rax, 10
      sub r8b, MIN
      add rax, r8
      inc rcx
      jmp .loop

   .end:
      mov rdx, rcx
      ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
   
    
parse_int:
    cmp byte [rdi],  ASCII_MINUS
    je handle_negative
    cmp byte [rdi],  ASCII_PLUS
    je handle_positive
    jmp parse_uint

handle_negative:
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret

handle_positive:
    inc rdi 
    jmp parse_uint
      

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
       xor rax, rax
       .loop: 
         cmp rax, rdx
         jge .failure
         mov dl, byte[rdi]
         mov byte[rsi], dl
         test dl, dl
         je .success
         inc rsi
         inc rdi
         jmp .loop
      .failure:
          xor rax, rax
      .success:
          ret

